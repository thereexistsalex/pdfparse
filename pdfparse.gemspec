Gem::Specification.new do |s|
  s.name        = 'pdfparse'
  s.version     = '1.0.0'
  s.date        = '2015-08-20'
  s.summary     = "PDF text and image extracter"
  s.description = "PDF text and image extracter written for mRendering"
  s.authors     = ["Alex Knapp"]
  s.email       = 'x.knapp@gmail.com'
  s.licenses    = ['private']
  s.files       = Dir.glob("{lib}/**/**/*")

  s.executables << 'pdfparse'

  s.add_dependency('pdf-reader', '~> 2')
  s.add_dependency('chunky_png', '~> 1')
end
