require 'pdf/reader'

module PDF
  class Reader
    class FormXObject
      attr_reader :objects

      def number
        @page.number
      end
    end
  end
end
