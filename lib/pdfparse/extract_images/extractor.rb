require 'pdf/reader'

require 'pdfparse/extract_images/jpg'
require 'pdfparse/extract_images/png'
require 'pdfparse/extract_images/raw'
require 'pdfparse/extract_images/tiff'

module ExtractImages
  class Extractor

    def extract_from_file(file, output_path)
      extract_and_yield(file) do |image, name|
        image.save(File.join(output_path + name))
      end
    end

    def extract_and_yield(io, &block)
      reader = PDF::Reader.new(io)

      reader.pages.each do |page|
        extract_from_page_yield(page, &block)
      end
    end

    def extract_from_page(page, path)
      process_page_yield(page, count) do |image, name|
        image.save(File.join(path + name))
      end
    end

    def extract_from_page_yield(page, &block)
      process_page_yield(page, 0, &block)
    end

    private

    def complete_refs
      @complete_refs ||= {}
    end

    def process_page_yield(page, count, &block)
      xobjects = page.xobjects
      return count if xobjects.empty?

      xobjects.each do |name, stream|
        case stream.hash[:Subtype]
        when :Image then
          count += 1

          case stream.hash[:Filter]
          when :CCITTFaxDecode then
            yield ExtractImages::Tiff.new(stream), "#{page.number}-#{count}-#{name}.tif"
          when :DCTDecode      then
            yield ExtractImages::Jpg.new(stream), "#{page.number}-#{count}-#{name}.jpg"
          when :FlateDecode  then
            if stream.hash[:ColorSpace] == :DeviceCMYK
              yield ExtractImages::Raw.new(stream), "#{page.number}-#{count}-#{name}.tif"
            else
              yield ExtractImages::Png.new(stream), "#{page.number}-#{count}-#{name}.png"
            end
          when :JPXDecode      then
            yield ExtractImages::Jpg.new(stream), "#{page.number}-#{count}-#{name}.jp2"
          else
            yield ExtractImages::Raw.new(stream), "#{page.number}-#{count}-#{name}.tif"
          end
        when :Form then
          count = process_page_yield(PDF::Reader::FormXObject.new(page, stream), count, &block)
        end
      end
      count
    end
  end
end
