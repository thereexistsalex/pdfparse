require 'pdf/reader'
require 'chunky_png'
require 'matrix'

module ExtractImages
  class Png
    attr_reader :stream

    def initialize(stream)
      @stream = stream
    end

    def save(filename)
      h    = stream.hash[:Height]
      w    = stream.hash[:Width]

      if h * w < 100
        puts "Image too small not saving"
        return
      end

      puts "#{filename}"

      png = to_datastream

      if png
        File.open(filename, "wb") { |file| file.write png}
      else
        $stderr.puts "unsupport color depth #{@stream.hash[:ColorSpace]} #{filename}"
      end
    end

    def to_datastream
      if @stream.hash[:ColorSpace]
        case @stream.hash[:ColorSpace][0]
        when :ICCBased then icc_datastream
        when :Indexed then indexed_datastream
        else
          nil
        end
      else
        bw_icc_datastream
      end
    end

    private

    def icc_datastream
      h    = stream.hash[:Height]
      w    = stream.hash[:Width]
      bpc  = stream.hash[:BitsPerComponent]
      len  = stream.hash[:Length]

      png = ChunkyPNG::Image.new(w, h)
      uncompressed_data = Zlib::Inflate.inflate(@stream.data).bytes

      color = if h * w == uncompressed_data.length
        :BlackWhite
      else
        :Color
      end

      (0...h).each do |h_i|
        (0...w).each do |w_i|


          if color == :BlackWhite
            total_i = (h_i * w + w_i)

            r = g = b = uncompressed_data[total_i]

            png[w_i, h_i] = ChunkyPNG::Color.rgb(r, g, b)
          else
            total_i = (h_i * w + w_i) * 3

            r = uncompressed_data[total_i]
            g = uncompressed_data[total_i + 1]
            b = uncompressed_data[total_i + 2]

            png[w_i, h_i] = ChunkyPNG::Color.rgb(r, g, b)
          end
        end
      end

      png.to_blob
    end

    def bw_icc_datastream
      h    = stream.hash[:Height]
      w    = stream.hash[:Width]
      bpc  = stream.hash[:BitsPerComponent]
      len  = stream.hash[:Length]

      png = ChunkyPNG::Image.new(w, h)
      uncompressed_data = Zlib::Inflate.inflate(@stream.data).bytes

      (0...h).each do |h_i|
        (0...w).each do |w_i|
          total_i = (h_i * w + w_i)
          png[w_i, h_i] = ChunkyPNG::Color.grayscale(total_i)
        end
      end

      png.to_blob
    end

    def indexed_datastream
      h    = stream.hash[:Height]
      w    = stream.hash[:Width]
      bpc  = stream.hash[:BitsPerComponent]
      len  = stream.hash[:Length]

      png = ChunkyPNG::Image.new(w, h)
      uncompressed_data = Zlib::Inflate.inflate(@stream.data).bytes

      color_data = if @stream.hash[:ColorSpace][3].class == PDF::Reader::Stream
        @stream.hash[:ColorSpace][3].data.bytes
      else
        @stream.hash[:ColorSpace][3].bytes
      end

      (0...h).each do |h_i|
        (0...w).each do |w_i|
          total_i = h_i * w + w_i

          reference_i = uncompressed_data[total_i]

          r = color_data[reference_i]
          g = color_data[reference_i + 1]
          b = color_data[reference_i + 2]
          png[w_i, h_i] = ChunkyPNG::Color.rgb(r, g, b)
        end
      end

      png.to_blob
    end
  end
end
