require 'pdf/reader'

module ExtractImages
  class Jpg
    attr_reader :stream

    def initialize(stream)
      @stream = stream
    end

    def save(filename)
      w = stream.hash[:Width]
      h = stream.hash[:Height]
      puts "#{filename}: h=#{h}, w=#{w}"
      File.open(filename, "wb") { |file| file.write to_datastream }
    end

    def to_datastream
      stream.data
    end
  end
end
