require 'pdf/reader'

module ExtractText
  class Extractor

    def extract_from_file(file, path)
      extract_and_yield(file) do |text|
        File.open(path, 'w') do |f|
          f.puts text
        end
      end
    end

    def extract_and_yield(io, &block)
      reader = PDF::Reader.new(io)

      text = ""

      reader.pages.each do |page|
        text << page.text << "\n"
      end

      text = text.lines.map{|line| line.strip}.reject{|line| line =~ /\A\d+\z/ }.join("\n")
      text = text.gsub("•", "+").gsub("▯", "").gsub(/\n\n\n+/, "\n\n")

      yield text
    end
  end
end
