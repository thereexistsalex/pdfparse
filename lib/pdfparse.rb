#!/usr/bin/env ruby
# coding: utf-8

# This demonstrates a way to extract some images (those based on the JPG or
# TIFF formats) from a PDF. There are other ways to store images, so
# it may need to be expanded for real world usage, but it should serve
# as a good guide.
#
# Thanks to Jack Rusher for the initial version of this example.

require 'pdf/reader'
require 'fileutils'
require 'chunky_png'

require 'pdfparse/pdf_reader'

require 'pdfparse/extract_images/extractor'
require 'pdfparse/extract_text/extractor'